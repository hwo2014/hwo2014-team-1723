package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import com.google.gson.Gson;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        double throttle = 0.8;
        int crash = 0;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            final MsgWithTick msg = gson.fromJson(line, MsgWithTick.class);
            
            if (msgFromServer.msgType.equals("carPositions")) {
                    int gameTick = msg.gameTick;
                    send(new Throttle(throttle), gameTick);
                    send(new SwitchLane("Right"), gameTick);
            } else {
                switch (msgFromServer.msgType) {
                case "join":
                    System.out.println("Joined");
                    break;
                case "gameInit":
                    System.out.println("Race init");
                    break;
                case "gameEnd":
                    System.out.println("Race end");
                    break;
                case "gameStart":
                    System.out.println("Race start");
                    send(new Throttle(0.0), 0);
                    break;
                case "crash":
                    throttle -= 0.1;
                    crash += 1;
                    break;
                case "lapFinished":
                    if (crash == 0) {
                        throttle += 0.05;
                    }
                    crash = 0;
                    break;
                default:
                    send(new Ping());
                    break;
                }
            }
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
    
    private void send (final SendMsg msg, int gameTick) {
        writer.println(msg.toJson(gameTick));
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }
    
    public String toJson(int gameTick) {
        return new Gson().toJson(new MsgWithTick(this, gameTick));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class MsgWithTick {
    public final String msgType;
    public final Object data;
    public final int gameTick;

    public MsgWithTick(String msgType, Object data, int gameTick) {
        this.msgType = msgType;
        this.data = data;
        this.gameTick = gameTick;
    }
    
    public MsgWithTick(final SendMsg sendMsg, int gameTick) {
        this(sendMsg.msgType(), sendMsg.msgData(), gameTick);
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;
    
    public SwitchLane(String value) {
        this.value = value;
    }
    
    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
